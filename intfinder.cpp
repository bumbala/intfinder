// Tolga Akyay (03/10/2017)

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>

// Compact and easy JSON library
// https://github.com/nlohmann/json
#include "json.hpp"

using namespace std;
using namespace nlohmann;

// Forward declarations
struct Intersection;
bool itemExists(Intersection& item);

// List off all intersections.
vector<Intersection> result;

// Defines a simple rectangle.
struct Rectangle
{
public:

	Rectangle(void) :
		id(0), left(0), right(0), bottom(0), top(0)
	{
	}
	
	Rectangle(int i, int x, int y, int w, int h) :
		id(i), left(x), bottom(y), right(x + w), top(y + h)
	{
	}

	int id;
	int left;
	int right;
	int bottom;
	int top;

	static Rectangle fromLBRT(int l, int b, int r, int t)
	{
		Rectangle rect;
		rect.left = l;
		rect.bottom = b;
		rect.right = r;
		rect.top = t;
		return rect;
	}

	int width() const
	{
		return right - left;
	}

	int height() const
	{
		return top - bottom;
	}

	bool intersects(Rectangle other)
	{
		return left < other.right
			&& right > other.left
			&& bottom < other.top
			&& top > other.bottom;
	}

	Rectangle intersection(Rectangle other)
	{
		return fromLBRT(
			max(left, other.left),
			max(bottom, other.bottom),
			min(right, other.right),
			min(top, other.top)
		);
	}
};

// Defines an intersection of one of more rectangles.
struct Intersection
{
public:

	Intersection() :
		ids(),
		overlap()
	{
	}

	vector<int> ids;
	Rectangle overlap;

	Intersection clone()
	{
		Intersection ix;
		ix.ids.reserve(ids.size());
		ix.ids.insert(ix.ids.begin(), ids.begin(), ids.end());
		ix.overlap = overlap;
		return ix;
	}

	// Checks whether specified id exist in this intersection.
	bool contains(int id)
	{
		return find(ids.begin(), ids.end(), id) != ids.end();
	}

	// If the specified rectangle intersects the current
	// overlap adds a new intersection to result.
	// This function is used for nested intersections.
	bool accept(Rectangle& rect)
	{
		if (contains(rect.id))
			return false;
		
		if (rect.intersects(overlap))
		{
			Intersection item = clone();
			item.ids.push_back(rect.id);
			item.overlap = rect.intersection(overlap);
			if (!itemExists(item))
			{
				result.push_back(item);
				return true;
			}
		}

		return false;
	}
};

// Checks whether specified intersection exists in current result.
bool itemExists(Intersection& item)
{
	vector<Intersection>::iterator ix;
	vector<int>::iterator id;

	for (ix = result.begin(); ix != result.end(); ix++)
	{
		if (ix->ids.size() != item.ids.size())
			continue;

		int count = 0;
		for (id = item.ids.begin(); id != item.ids.end(); id++)
		{
			if (ix->contains(*id))
				count++;
		}

		if (count == item.ids.size())
			return true;
	}

	return false;
}

int main(int argc, char* argv[])
{
	vector<Rectangle>::iterator r1, r2;
	vector<Intersection>::iterator ix;

	// Elapsed time
	clock_t startTime = clock();

	// Check whether a file name is supplied.
	if (argc < 2)
	{
		cout << "Please specify a JSON file." << endl;
		return 1;
	}

	// Open JSON file.
	ifstream f(argv[1]);

	// Check whether specified file exists.
	if (!f.good())
	{
		cout << "Specified JSON file does not exist." << endl;
		return 1;
	}

	// Parse JSON file.
	json j = json::parse(f, NULL, false);

	// Check if JSON file is valid.
	if (j.is_discarded() || !j["rects"].size())
	{
		cout << "Specified JSON file is invalid." << endl;
		return 1;
	}

	// Read the rectangles from JSON.
	vector<Rectangle> rectangles;
	for (size_t i = 0; i < j["rects"].size(); ++i)
	{
		json row = j["rects"][i];
		int id = i + 1;

		// Check if the rectangle is valid.
		if (!row["x"].is_number_integer() ||
			!row["y"].is_number_integer() ||
			!row["w"].is_number_integer() ||
			!row["h"].is_number_integer())
		{
			cout << "Rectangle " << id << ": Invalid" << endl;
			continue;
		}

		int x = row["x"];
		int y = row["y"];
		int w = row["w"];
		int h = row["h"];

		if (w <= 0)
		{
			cout << "Rectangle " << id << ": Zero or negative width" << endl;
			continue;
		}

		if (h <= 0)
		{
			cout << "Rectangle " << id << ": Zero or negative height" << endl;
			continue;
		}

		rectangles.push_back(Rectangle(id, x, y, w, h));
	}

	// If no intersections found, bail out.
	if (!rectangles.size())
	{
		cout << "No valid rectangles found." << endl;
		return 1;
	}

	// Dump input rectangles.
	cout << "Input:" << endl;
	for (r1 = rectangles.begin(); r1 != rectangles.end(); r1++)
	{
		cout << "\t" << r1->id << ": Rectangle at (" << r1->left << "," << r1->bottom << "), "
			 << "w=" << r1->width() << ", h=" << r1->height() << "." << endl;
	}
	cout << endl;

	// First Pass: Add pairs of intersecting rectangles.
	for (r1 = rectangles.begin(); r1 != rectangles.end(); r1++)
	{
		for (r2 = rectangles.begin(); r2 != rectangles.end(); r2++)
		{
			if (r1->id == r2->id)
				continue;

			if (!r1->intersects(*r2))
				continue;

			Intersection item;
			item.ids.push_back(r1->id);
			item.ids.push_back(r2->id);
			item.overlap = r1->intersection(*r2);
			if (!itemExists(item))
				result.push_back(item);
		}
	}


	// Second Pass: Check for nested intersections.
	for (r1 = rectangles.begin(); r1 != rectangles.end(); r1++)
	{
		bool changed = true;
		while (changed)
		{
			changed = false;
			for (ix = result.begin(); ix != result.end(); ix++)
			{
				if (!ix->contains(r1->id) && ix->accept(*r1))
				{
					// Result changed, start from beginning.
					changed = true;
					break;
				}
			}
		}
	}

	// If no intersections found, bail out.
	if (!result.size())
	{
		cout << "No intersections found." << endl;
		return 0;
	}

	// Dump resulting intersections.
	cout << "Intersections:" << endl;
	for (ix = result.begin(); ix != result.end(); ix++)
	{
		// Sort IDs to display result nice and in order.
		sort(ix->ids.begin(), ix->ids.end(), [](const int& a, const int& b) {
			return a < b;
		});

		cout << "\tBetween rectangle ";
		for (size_t i = 0; i < ix->ids.size(); ++i)
		{
			if (i > 0)
			{
				if (i == ix->ids.size() - 1)
					cout << " and ";
				else
					cout << ", ";
			}
			cout << ix->ids[i];
		}

		Rectangle& rect = ix->overlap;
		cout << " at (" << rect.left << "," << rect.bottom << "), "
			 << "w=" << rect.width() << ", h=" << rect.height() << "." << endl;
	}

	// Dump elapsed seconds.
	clock_t endTime = clock();
	double elapsed = double(endTime - startTime) / CLOCKS_PER_SEC;
	cout << endl << "Completed at " << elapsed << " seconds." << endl;

	return 0;
}